import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:xkcd/models/comic.dart';

void main() {
  group('malformed initialization throws exception', () {
    test('when comic num <= 0', () {
      expect(() => Comic(num: 0, title: 'a', alt: 'a', img: 'a'),
          throwsAssertionError);
      expect(() => Comic(num: -2, title: 'a', alt: 'a', img: 'a'),
          throwsAssertionError);
      expect(() => Comic(num: -500000, title: 'a', alt: 'a', img: 'a'),
          throwsAssertionError);
    });

    test('when comic has no title', () {
      expect(() => Comic(num: 2, title: ' ', alt: 'a', img: 'a'),
          throwsAssertionError,
          reason: 'no title');
      expect(() => Comic(num: 2, title: '', alt: 'a', img: 'a'),
          throwsAssertionError,
          reason: 'no title');
      expect(() => Comic(num: 2, title: null, alt: 'a', img: 'a'),
          throwsAssertionError,
          reason: 'null title');
    });

    test('when comic has no alt text', () {
      expect(() => Comic(num: 2, title: 'a', alt: '', img: 'a'),
          throwsAssertionError,
          reason: 'no alt');
      expect(() => Comic(num: 2, title: 'a', alt: ' ', img: 'a'),
          throwsAssertionError,
          reason: 'no alt');
      expect(() => Comic(num: 2, title: 'a', alt: null, img: 'a'),
          throwsAssertionError,
          reason: 'null alt');
    });
    test('when comic has no img url', () {
      expect(() => Comic(num: 2, title: 'a', alt: 'a', img: ''),
          throwsAssertionError,
          reason: 'no url');
      expect(() => Comic(num: 2, title: 'a', alt: 'a', img: ' '),
          throwsAssertionError,
          reason: 'no url');
      expect(() => Comic(num: 2, title: 'a', alt: 'a', img: null),
          throwsAssertionError,
          reason: 'null url');
    });
  });

  group('JSON parsing', () {
    var payload =
        '{"num":1337, "alt": "This is a test", "img": "testimg", "title": "Test"}';
    var badPayload = '{"num":1337,}';

    test('good json payload', () {
      final expectedNum = 1337;
      final expectedAlt = 'This is a test';
      final expectedTitle = 'Test';
      final expectedImg = 'testimg';
      final parsed = Comic.fromJson(jsonDecode(payload));
      expect(parsed.num, equals(expectedNum));
      expect(parsed.alt, equals(expectedAlt));
      expect(parsed.title, equals(expectedTitle));
      expect(parsed.img, equals(expectedImg));
    });

    test('bad json payload causes format exception', () {
      expect(
          () => Comic.fromJson(jsonDecode(badPayload)), throwsFormatException);
    });
  });

  test('equality testing', () {
    var source =
        '{"num":1337, "alt": "alt", "img": "testimg", "title": "Test"}';
    var a = Comic.fromJson(jsonDecode(source));
    var b = Comic.fromJson(jsonDecode(source));
    var c = Comic(num: 1337, alt: 'alt', img: 'testimg', title: 'Test');

    var reflexive = a == a;
    var symmetric = a == b && b == a;
    var transitive = (a == b && b == c) && a == c;

    expect(reflexive, isTrue);
    expect(symmetric, isTrue);
    expect(transitive, isTrue);
  });

  test('Hashing', () {
    var comic = Comic(num: 1337, alt: 'alt', img: 'testimg', title: 'Test');
    var another = Comic.notFound();

    expect(comic.hashCode == another.hashCode, isFalse);
  });
}
