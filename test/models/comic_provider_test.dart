import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:xkcd/models/comic.dart';
import 'package:xkcd/models/comic_provider.dart';
import 'package:xkcd/service/comic_service.dart';

class MockService extends Mock implements ComicService {}

final _fakeComic = Comic(alt: 'a', img: 'img', num: 5, title: 'title');
final _firstComic = Comic(alt: 'a', img: 'img', num: 1, title: 'title');

void main() {
  ComicProvider provider;
  ComicService mock;

  setUp(() async {
    when(mock.getLatest()).thenAnswer((_) => Future.value(_fakeComic));
    provider.current;
    await provider.latest(); // initial call of the provider
    clearInteractions(mock);
  });

  tearDown(() {
    clearInteractions(mock);
    resetMockitoState();
  });

  setUpAll(() {
    mock = MockService();
    provider = ComicProvider(mock);
  });
  tearDownAll(() {
    mock = null;
    provider = null;
  });

  test('Test suit is valid', () {
    expect(provider.current, isNotNull);

    verifyZeroInteractions(mock);
    resetMockitoState();
  });

  group('Current comic book', () {
    test('will not do extra calls for the service until being changed', () {
      // call five times
      for (var t = 0; t < 5; t++) {
        provider.current;
      }
      verifyZeroInteractions(mock);
    });

    test('do not increase service call count', () {
      when(mock.getFirst()).thenAnswer((_) => Future.value(_firstComic));
      provider.first();
      provider.current;

      verify(mock.getFirst());
      verifyNoMoreInteractions(mock);
    });

    test('increase count on forced update', () async {
      provider.forceUpdate();
      provider.current;
      verify(mock.getLatest());
      verifyNoMoreInteractions(mock);
    });
  });

  group('Next calls', () {
    test('the next comic if is not the latest', () async {
      when(mock.getFirst()).thenAnswer((_) => Future.value(_firstComic));
      when(mock.getById(any)).thenAnswer((_) => Future.value(_firstComic));

      await provider.first(); // will call latest and getById once
      verify(mock.getFirst()).called(1);

      await provider.next(); // will call latest again and getById once
      verify(mock.getById(any)).called(1);
      verifyNoMoreInteractions(mock);
    });

    test('will not ping service if the current is the latest', () async {
      await provider.next();
      await provider.next();
      verifyNoMoreInteractions(mock);
    });

    test('next on the latest will not call by id', () async {
      when(mock.getLatest()).thenAnswer((_) => Future.value(_fakeComic));
      await provider.current;
      await provider.next();

      verifyNever(mock.getLatest());
      verifyNoMoreInteractions(mock);
    });
  });

  group('Previous call', () {
    test('do nothing if it is already the first comic', () async {
      when(mock.getFirst()).thenAnswer((_) => Future.value(_firstComic));
      await provider.first();
      await provider.previous();
      verify(mock.getFirst());
      verifyNoMoreInteractions(mock);
    });

    test('service is called if not first comic', () async {
      when(mock.getById(4)).thenAnswer((_) => Future.value(_firstComic));
      await provider.previous();

      verify(mock.getById(4));
      verifyNoMoreInteractions(mock);
    });
  });

  group('Random call', () {
    test('any number between 1 and max', () async {
      when(mock.getById(captureAny))
          .thenAnswer((_) => Future.value(_firstComic));
      await provider.random();

      int id = verify(mock.getById(captureAny)).captured.first;
      if (id < 5) {
        expect(id, inInclusiveRange(1, 4));
        verifyNoMoreInteractions(mock);
      } else {
        verifyZeroInteractions(mock);
      }
    });
  });

  test('Force update', () {
    provider.forceUpdate();
    verify(mock.getLatest());
    verifyNoMoreInteractions(mock);
  });
}
