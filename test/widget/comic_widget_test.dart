import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:xkcd/widgets/comic_widget.dart';

import 'test_builders.dart';

void main() {
  group('ComicTitle widget test', () {
    testWidgets('it expands if title text is pressed',
        (WidgetTester tester) async {
      await tester.pumpWidget(
        withMaterial(child: ComicTitle(title: 'Title', alt: 'Alt')),
      );
      await tester.tap(find.text('Title'));
      await tester.pumpAndSettle();
      expect(find.byType(Text), findsNWidgets(2));
      await tester.tap(find.text('Title'));
    });
    testWidgets('it contracts if the title is pressed again',
        (WidgetTester tester) async {
      await tester.pumpWidget(
        withMaterial(child: ComicTitle(title: 'Title', alt: 'Alt')),
      );
      await tester.tap(find.text('Title'));
      await tester.pumpAndSettle();
      expect(find.byType(Text), findsNWidgets(2));
      await tester.tap(find.text('Title'));
      await tester.pumpAndSettle();
      expect(find.byType(Text), findsNWidgets(1));
    });
  });
}
