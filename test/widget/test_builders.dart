import 'package:flutter/material.dart';

/// Generate the widget under test wraped with Material, Material App, Scaffold
/// and a SingleChildScrollView with directionality for text, if any
/// Requires a non-null child Widget to be wrapped.
Widget withMaterial({@required Widget child}) => Material(
      child: MaterialApp(
        home: Scaffold(
          body: SingleChildScrollView(
            child:
                Directionality(textDirection: TextDirection.ltr, child: child),
          ),
        ),
      ),
    );
