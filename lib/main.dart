import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:xkcd/service/comic_service.dart';
import 'models/comic_provider.dart';
import 'views/home_view.dart';
import 'widgets/theme.dart';

void main() => runApp(ChangeNotifierProvider(
      create: (context) => ComicProvider(ComicService()),
      child: MyApp(),
    ));

/// Point of entry Widget to call the Material App, with the first and single
/// widget followed by the theme
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomeScreen(),
      theme: xkcdTheme(),
    );
  }
}
