import 'package:flutter/material.dart';

/// Primary color is grey purple #6E7B91
const primaryColor = Color(0xff6E7B91);

/// Accent color is pure black #000
const accentColor = Colors.black;

/// xkcd name
const name = 'xkcd';

/// subtitle of xkcd comics
const description = 'A webcomic of romance, sarcasm, math, and language.';
// Api Constants
/// Point of entry
const apiUrl = 'https://xkcd.com';

/// end of the string for all api call
const apiPredicate = '/info.0.json';
