import 'dart:collection';
import 'dart:math';

import 'package:flutter/material.dart';
import '../service/comic_service.dart';
import 'comic.dart';

/// The provider requires that after instantiation to call current comic to
/// update internal state
class ComicProvider extends ChangeNotifier {
  /// Creates an instance of the provider, if any, and returns it.
  /// If no service is provided, the ComicService is automatically injected
  factory ComicProvider([ComicService service]) {
    _instance ?? ComicProvider._(service);
    return _instance;
  }

  /// holds the instance of the provider
  static ComicProvider _instance;

  ComicService _service;

  /// Constructor that is called only once in the lifecycle. This will inject the
  /// desired Service into the Provider.
  ComicProvider._([service = const ComicService()]) {
    _service = service;
    _instance = this;
  }

  /// cache map that will reduce api calls.
  final Map<int, Comic> _cache = HashMap<int, Comic>();

  /// The current comic being displayed
  Comic _current;

  /// keeps track on initial build of the max value of the
  int _max;

  /// This is the most current comic in XKCD server
  Comic get current {
    if (_current == null) {
      _init();
    }
    return _current;
  }

  void _init() async {
    await _service.getLatest().then((value) {
      _current = value;
      _updateCache(_current);
      _max = _current.num;
    });

    notifyListeners();
  }

  /// get the first comic (id 1) from XKCD
  void first() async => await _service.getFirst().then((value) {
        _updateCache(value);
        _current = value;
      }).whenComplete(() => notifyListeners());

  /// get the latest comic in XKCD server
  Future<void> latest() async {
    var res = await _service.getLatest();

    if (res.num > _max) {
      _max = _current.num;
      _updateCache(res);
    }

    _current = res;
    notifyListeners();
  }

  /// get the previous comic from this. if the actual is 1, it will do nothing
  Future<void> previous() async {
    final prev = _current.num - 1;

    if (prev >= 1) {
      if (!_cache.containsKey(prev)) {
        _current = await _service.getById(prev);
        _updateCache(_current);
      } else {
        _current = _cache[prev];
      }
      notifyListeners();
    }
  }

  // will lookup for the next. if the actual is the latest, it will keep calling xkcd for the latest
  Future<void> next() async {
    final next = _current.num + 1;

    if (next <= _max) {
      if (!_cache.containsKey(next)) {
        _current = await _service.getById(next);
        _updateCache(_current);
      } else {
        _current = _cache[next];
      }
      notifyListeners();
    }
    return;
  }

  /// get a random xkcd comic based on the max value known by the provider.
  Future<void> random() async {
    final random = Random().nextInt(_max) + 1;

    if (!_cache.containsKey(random)) {
      await _service.getById(random).then((value) {
        _current = value;
        _updateCache(value);
      });
    } else {
      _current = _cache[random];
    }
    notifyListeners();
  }

  void forceUpdate() => _updateMax();

  /// updates the max known value
  Future<void> _updateMax() async => _max = await _service
      .getLatest()
      .then((value) => value.num)
      .whenComplete(() => notifyListeners());

  /// Update the cache with the data, otherwise
  void _updateCache(Comic toCache) => (!_cache.containsKey(toCache.num))
      ? _cache[toCache.num] = toCache
      : false;
}
