import 'dart:convert';

import 'package:http/http.dart';

import '../models/comic.dart';
import '../api/api_calls.dart' as api;

class ComicService {
  /// Creates a ComicService
  const ComicService();

  /// Retrieves from XKCD server the latest comic
  Future<Comic> getLatest() async {
    try {
      final response = await api.getLatest();
      return _handleResponse(response);
    } catch (e) {
      // log error

    }
    return Comic.notFound();
  }

  /// Retrieves from XKCD server for a specific comic using its number
  Future<Comic> getById([int id = 1]) async {
    if (id < 1) {
      id = 1;
    }
    try {
      final res = await api.getById(id);
      return _handleResponse(res);
    } catch (e) {
      // log error

    }
    return Comic.notFound();
  }

  /// Retrieves from XKCD server the comic with Id #1
  Future<Comic> getFirst() => getById(1);

  /// Generic handle of all responses
  Comic _handleResponse(Response response) {
    if (response.statusCode >= 500) {
      // Need to create a comic for Error 500
      return Comic.notFound();
    } else if (response.statusCode > 400 && response.statusCode < 500) {
      return Comic.notFound();
    } else {
      return Comic.fromJson(json.decode(response.body));
    }
  }
}
