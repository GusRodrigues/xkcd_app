import 'package:http/http.dart' as http;
import '../constants/constants.dart';

/// XKCD public API is open for procedural GET call only
/// The point of entry to the api is on the constants file as [apiUrl],
/// followed by / and the number of the comic, another / and with the [apiPredicate].
///
/// The API methods will always return a [http.Response]

/// Call the API for the newest xkcd comic
Future<http.Response> getLatest() async =>
    await http.get('$apiUrl$apiPredicate');

/// Call the API for a xckd comic by ID, where the the ID >= 1
Future<http.Response> getById([int id = 1]) async {
  final search = '$apiUrl/$id$apiPredicate';
  return await http.get(search);
}
