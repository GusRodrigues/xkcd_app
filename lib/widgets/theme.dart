import 'package:flutter/material.dart';
import '../constants/constants.dart';

ThemeData xkcdTheme() => ThemeData(
      primaryColor: primaryColor,
      accentColor: accentColor,
      typography: Typography.material2018(),
      primaryColorBrightness: Brightness.dark, // white text
      accentColorBrightness: Brightness.dark, // white text
      appBarTheme: _appBarTheme,
      textTheme: TextTheme().copyWith(
        headline6: TextStyle(color: Colors.white),
        subtitle2: TextStyle(color: Colors.white),
      ),
    );

/// Theme of the app bar
final AppBarTheme _appBarTheme = AppBarTheme().copyWith(
  color: primaryColor,
  elevation: 3,
  brightness: Brightness.dark, // text is white
);
