import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:xkcd/models/comic_provider.dart';

class ComicNavigation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Wrap(
      crossAxisAlignment: WrapCrossAlignment.center,
      direction: Axis.horizontal,
      runSpacing: 5,
      alignment: WrapAlignment.spaceEvenly,
      textDirection: TextDirection.ltr,
      children: <Widget>[
        NavButton(
          action: '|<',
          onPressed: () =>
              Provider.of<ComicProvider>(context, listen: false).first(),
        ),
        NavButton(
          action: '<',
          onPressed: () =>
              Provider.of<ComicProvider>(context, listen: false).previous(),
        ),
        NavButton(
          action: '?',
          onPressed: () =>
              Provider.of<ComicProvider>(context, listen: false).random(),
        ),
        NavButton(
          action: '>',
          onPressed: () =>
              Provider.of<ComicProvider>(context, listen: false).next(),
        ),
        NavButton(
          action: '>|',
          onPressed: () =>
              Provider.of<ComicProvider>(context, listen: false).latest(),
        ),
      ],
    );
  }
}

class NavButton extends StatelessWidget {
  final String action;
  final VoidCallback onPressed;

  const NavButton({
    Key key,
    @required this.action,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 5.0),
      constraints: BoxConstraints(
          minHeight: 40, maxHeight: 40, minWidth: 40, maxWidth: 40),
      child: FlatButton(
        visualDensity: VisualDensity.compact,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
          side: BorderSide(color: Colors.black),
        ),
        onPressed: onPressed,
        child: Text(
          action,
          style: Theme.of(context).textTheme.headline6,
        ),
        textColor: Colors.white,
        color: Theme.of(context).primaryColor,
      ),
    );
  }
}
