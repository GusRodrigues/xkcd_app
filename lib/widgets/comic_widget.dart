import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../constants/constants.dart';
import '../models/comic_provider.dart';
import '../models/comic.dart';
import 'navigation_widget.dart';

class ComicView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<ComicProvider>(
      builder: (context, provider, _) {
        var comic = provider.current;
        return (comic == null) ? LoadingWidget() : ComicPresenter(data: comic);
      },
    );
  }
}

class LoadingWidget extends StatelessWidget {
  const LoadingWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(8),
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            CircularProgressIndicator(),
          ],
        ),
      ),
    );
  }
}

class ComicPresenter extends StatelessWidget {
  final Comic data;

  const ComicPresenter({Key key, this.data}) : super(key: key);

  Widget _comicImage(Comic data) => (data.num == 404)
      ? Text('404',
          textDirection: TextDirection.ltr,
          style: TextStyle(
            color: primaryColor,
            fontSize: 64,
          ))
      : Image.network(data.img);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 4),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        textDirection: TextDirection.ltr,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _comicImage(data),
          ComicTitle(
            title: '${data.title}',
            alt: '${data.alt}',
          ),
          ComicNavigation(),
        ],
      ),
    );
  }
}

class ComicTitle extends StatelessWidget {
  ComicTitle({
    Key key,
    @required this.title,
    @required this.alt,
  }) : super(key: key);

  final String title;
  final String alt;

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      key: Key(alt),
      title: Center(
        child: Text(
          title,
          textDirection: TextDirection.ltr,
          style: TextStyle().copyWith(
            color: Theme.of(context).primaryColor,
            fontWeight: FontWeight.bold,
            fontSize: 22,
          ),
        ),
      ),
      children: <Widget>[
        Container(
          padding: const EdgeInsets.all(8.0),
          margin: const EdgeInsets.all(8.0),
          decoration: BoxDecoration(
            // TODO remove this color to the theme data
            color: Color(0xFFFFF9BD),
            border: Border.all(),
          ),
          child: Text(alt, textDirection: TextDirection.ltr),
        )
      ],
    );
  }
}
